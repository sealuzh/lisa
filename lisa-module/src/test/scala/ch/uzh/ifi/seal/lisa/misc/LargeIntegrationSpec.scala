package ch.uzh.ifi.seal.lisa.misc

import test.scala.ch.uzh.ifi.seal.lisa.LisaSpec
import ch.uzh.ifi.seal.lisa.core.Parser
import ch.uzh.ifi.seal.lisa.module.parser.AntlrJavaParseTree
import ch.uzh.ifi.seal.lisa.module.parser.AntlrJavaParser

class LargeIntegrationSpec extends LisaSpec("largeIntegrationSpec") {

  sequential

  implicit override val domain = AntlrJavaParseTree
  val c = makeComputation(List[Parser](AntlrJavaParser))

  basicTest(c)

}
