package ch.uzh.ifi.seal.lisa.parser

import scala.collection.mutable._
import java.util.concurrent.ConcurrentLinkedQueue

import test.scala.ch.uzh.ifi.seal.lisa.LisaSpec
import ch.uzh.ifi.seal.lisa.core._
import ch.uzh.ifi.seal.lisa.module.parser.AntlrPython3Parser
import ch.uzh.ifi.seal.lisa.module.parser.AntlrPython3ParseTree
import ch.uzh.ifi.seal.lisa.module.analysis.MccAnalysis.Mcc
import ch.uzh.ifi.seal.lisa.module.analysis.NameAnalysis.Name
import ch.uzh.ifi.seal.lisa.module.analysis.UniquePathsAnalysis.UniquePaths

class AntlrPython3ComputationSpec extends LisaSpec("python3Spec") {

  implicit override val domain = AntlrPython3ParseTree
  val c = makeComputation(List[Parser](AntlrPython3Parser))

  "The small computation should" >> {

    prepare(c)
    compute(c)

    "produce correct number of class aggregates" in {
      classAggregates = getClassAggregates(c.graph)
      logger.debug(s"class aggregates: ${classAggregates.size}")
      assert(classAggregates.size == 12)
      success
    }

    "calculate correct NOM measures for classes" in {
      logger.debug("checking NOM")
      val expectedValues = Map(
        "Main" ->                 Map("[0-0]" -> 1),
        "CustomException" ->      Map("[0-0]" -> 1),
        "ConcreteClass" ->        Map("[0-0]" -> 9),
        "ConcreteSubclass" ->     Map("[0-0]" -> 1),
        "ControlFlow" ->          Map("[0-0]" -> 22),
        "InnerClass" ->           Map("[0-0]" -> 1),
        "InnerInnerClass" ->      Map("[0-0]" -> 2),
        "SubpackageClass" ->      Map("[0-0]" -> 1),
        "SubsubpackageClass" ->   Map("[0-0]" -> 1),
        "CodeSmell" ->            Map("[0-0]" -> 0),
        "BrainMethod" ->          Map("[0-0]" -> 1),
        "SiblingClass" ->         Map("[0-0]" -> 1)
      )
      checkValuesInt(classAggregates, expectedValues,
                         { case (name, range, nom, _, _, _, _) => (name, range, nom)})
      success
    }

    "calculate correct NumPara measures for classes" in {
      logger.debug("checking NumPara")
      val expectedValues = Map(
        "Main" ->                 Map("[0-0]" -> 1),
        "CustomException" ->      Map("[0-0]" -> 3),
        "ConcreteClass" ->        Map("[0-0]" -> 20),
        "ConcreteSubclass" ->     Map("[0-0]" -> 1),
        "ControlFlow" ->          Map("[0-0]" -> 22),
        "InnerClass" ->           Map("[0-0]" -> 1),
        "InnerInnerClass" ->      Map("[0-0]" -> 2),
        "SubpackageClass" ->      Map("[0-0]" -> 1),
        "SubsubpackageClass" ->   Map("[0-0]" -> 1),
        "CodeSmell" ->            Map("[0-0]" -> 0),
        "BrainMethod" ->          Map("[0-0]" -> 1),
        "SiblingClass" ->         Map("[0-0]" -> 1)
      )
      checkValuesInt(classAggregates, expectedValues,
                         { case (name, range, _, numpara, _, _, _) => (name, range, numpara)})
      success
    }

    "calculate correct statement measures for classes" in {
      logger.debug("checking statements")
      val expectedValues = Map(
        "Main" ->                 Map("[0-0]" -> 32),
        "CustomException" ->      Map("[0-0]" -> 2),
        "ConcreteClass" ->        Map("[0-0]" -> 21),
        "ConcreteSubclass" ->     Map("[0-0]" -> 1),
        "ControlFlow" ->          Map("[0-0]" -> 76),
        "InnerClass" ->           Map("[0-0]" -> 1),
        "InnerInnerClass" ->      Map("[0-0]" -> 4),
        "SubpackageClass" ->      Map("[0-0]" -> 2),
        "SubsubpackageClass" ->   Map("[0-0]" -> 2),
        "CodeSmell" ->            Map("[0-0]" -> 1),
        "BrainMethod" ->          Map("[0-0]" -> 23),
        "SiblingClass" ->         Map("[0-0]" -> 2)
      )
      checkValuesInt(classAggregates, expectedValues,
                         { case (name, range, _, _, _, _, stmts) => (name, range, stmts)})
      success
    }

    "calculate correct NOV measures for classes" in {
      logger.debug("checking NOV")
      val expectedValues = Map(
        "Main" ->                 Map("[0-0]" -> 17),
        "CustomException" ->      Map("[0-0]" -> 4),
        "ConcreteClass" ->        Map("[0-0]" -> 32),
        "ConcreteSubclass" ->     Map("[0-0]" -> 1),
        "ControlFlow" ->          Map("[0-0]" -> 28),
        "InnerClass" ->           Map("[0-0]" -> 3),
        "InnerInnerClass" ->      Map("[0-0]" -> 2),
        "SubpackageClass" ->      Map("[0-0]" -> 3),
        "SubsubpackageClass" ->   Map("[0-0]" -> 3),
        "CodeSmell" ->            Map("[0-0]" -> 0),
        "BrainMethod" ->          Map("[0-0]" -> 16),
        "SiblingClass" ->         Map("[0-0]" -> 3)
      )
      checkValuesInt(classAggregates, expectedValues,
                         { case (name, range, _, _, nov, _, _) => (name, range, nov)})
      success
    }

    "calculate correct uniquePaths measures for methods of the ControlFlow class" in {
      logger.debug("checking uniquePaths")
      val measurements = new ConcurrentLinkedQueue[(String,RevisionRange,Int,Int,Int,BigInt,Int)]()
      c.graph.foreachVertex {
        case v: AstVertex =>
          var interesting = v.id.contains("ControlFlow") &&
            v.state.rangeStates.values.exists { value =>
              value.is('method)
            }
          if (interesting) {
            v.state.rangeStates.foreach { case (range, state) =>
              val name = state[Name].name
              measurements.add(name, range, 0, 0, 0, state[UniquePaths].n, 0)
            }
          }
        case _ =>
      }
      val expectedValues = Map(
        "singleIf" ->                               Map("[0-0]" -> BigInt(2)),
        "twoNestedIfs" ->                           Map("[0-0]" -> BigInt(3)),
        "twoConsecutiveIfs" ->                      Map("[0-0]" -> BigInt(4)),
        "simpleIfTree" ->                           Map("[0-0]" -> BigInt(8)),
        "complexIfTree" ->                          Map("[0-0]" -> BigInt(24)),
        "orOperator" ->                             Map("[0-0]" -> BigInt(2)),
        "orOperatorInsideIf" ->                     Map("[0-0]" -> BigInt(3)),
        "twoOrOperatorsInsideIf" ->                 Map("[0-0]" -> BigInt(4)),
        "ternaryOperator" ->                        Map("[0-0]" -> BigInt(2)),
        "ternaryOperatorInsideIf" ->                Map("[0-0]" -> BigInt(3)),
        "singleFor" ->                              Map("[0-0]" -> BigInt(2)),
        "twoNestedFors" ->                          Map("[0-0]" -> BigInt(3)),
        "twoConsecutiveFors" ->                     Map("[0-0]" -> BigInt(4)),
        "singleForWithContinue" ->                  Map("[0-0]" -> BigInt(2)),
        "singleWhile" ->                            Map("[0-0]" -> BigInt(2)),
        "twoNestedWhiles" ->                        Map("[0-0]" -> BigInt(3)),
        "consecutiveWhiles" ->                      Map("[0-0]" -> BigInt(4)),
        "singleWhileWithBreak" ->                   Map("[0-0]" -> BigInt(2)),
        "singleWhileWithContinue" ->                Map("[0-0]" -> BigInt(2)),
        "complexMixedExample" ->                    Map("[0-0]" -> BigInt(56)),
        "listComprehension" ->                      Map("[0-0]" -> BigInt(1)),
        "dictComprehension" ->                      Map("[0-0]" -> BigInt(1))
      )
      checkValues(measurements.toArray(Array[(String,RevisionRange,Int,Int,Int,BigInt,Int)]()).toList, expectedValues,
                         { case (name, range, _, _, _, uniquePaths, _) => (name, range, uniquePaths)})
      success
    }

    "calculate correct MCC measures for methods of the ControlFlow class" in {
      logger.debug("checking ControlFlow MCC")
      val measurements = new ConcurrentLinkedQueue[(String,RevisionRange,Int,Int,Int,BigInt,Int)]()
      c.graph.foreachVertex {
        case v: AstVertex =>
          var interesting = v.id.contains("ControlFlow") &&
            v.state.rangeStates.values.exists { value =>
              value.is('method)
            }
          if (interesting) {
            v.state.rangeStates.foreach { case (range, state) =>
              val name = state[Name].name
              measurements.add(name, range, 0, 0, 0, state[Mcc].n, 0)
            }
          }
        case _ =>
      }
      val expectedValues = Map(
        "singleIf" ->                               Map("[0-0]" -> 2),
        "twoNestedIfs" ->                           Map("[0-0]" -> 3),
        "twoConsecutiveIfs" ->                      Map("[0-0]" -> 3),
        "simpleIfTree" ->                           Map("[0-0]" -> 5),
        "complexIfTree" ->                          Map("[0-0]" -> 7),
        "orOperator" ->                             Map("[0-0]" -> 2),
        "orOperatorInsideIf" ->                     Map("[0-0]" -> 3),
        "twoOrOperatorsInsideIf" ->                 Map("[0-0]" -> 4),
        "ternaryOperator" ->                        Map("[0-0]" -> 2),
        "ternaryOperatorInsideIf" ->                Map("[0-0]" -> 3),
        "singleFor" ->                              Map("[0-0]" -> 2),
        "twoNestedFors" ->                          Map("[0-0]" -> 3),
        "twoConsecutiveFors" ->                     Map("[0-0]" -> 3),
        "singleForWithContinue" ->                  Map("[0-0]" -> 2),
        "singleWhile" ->                            Map("[0-0]" -> 2),
        "twoNestedWhiles" ->                        Map("[0-0]" -> 3),
        "consecutiveWhiles" ->                      Map("[0-0]" -> 3),
        "singleWhileWithBreak" ->                   Map("[0-0]" -> 2),
        "singleWhileWithContinue" ->                Map("[0-0]" -> 2),
        "complexMixedExample" ->                    Map("[0-0]" -> 9),
        "listComprehension" ->                      Map("[0-0]" -> 1),
        "dictComprehension" ->                      Map("[0-0]" -> 1)
      )
      checkValuesInt(measurements.toArray(Array[(String,RevisionRange,Int,Int,Int,BigInt,Int)]()).toList, expectedValues,
                         { case (name, range, _, _, _, mcc, _) => (name, range, mcc)})
      success
    }


    "calculate correct MCC measures for classes" in {
      logger.debug("checking class MCC")
      val expectedValues = Map(
        "Main" ->                 Map("[0-0]" -> 6),
        "CustomException" ->      Map("[0-0]" -> 1),
        "ConcreteClass" ->        Map("[0-0]" -> 5),
        "ConcreteSubclass" ->     Map("[0-0]" -> 1),
        "ControlFlow" ->          Map("[0-0]" -> 46),
        "InnerClass" ->           Map("[0-0]" -> 2),
        "InnerInnerClass" ->      Map("[0-0]" -> 2),
        "SubpackageClass" ->      Map("[0-0]" -> 1),
        "SubsubpackageClass" ->   Map("[0-0]" -> 1),
        "CodeSmell" ->            Map("[0-0]" -> 1),
        "BrainMethod" ->          Map("[0-0]" -> 7),
        "SiblingClass" ->         Map("[0-0]" -> 1)
      )
      checkValuesInt(classAggregates, expectedValues,
                         { case (name, range, _, _, _, mcc, _) => (name, range, mcc)})
      success
    }

    persist(c)
    finalize(c)

  }

}

