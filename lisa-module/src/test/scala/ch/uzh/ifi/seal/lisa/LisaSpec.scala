package test.scala.ch.uzh.ifi.seal.lisa

import java.util.concurrent.ConcurrentLinkedQueue
import com.typesafe.config.ConfigFactory
import org.specs2.mutable.SpecificationLike
import org.specs2.matcher.Matcher
import scala.collection.mutable._
import com.signalcollect._
import com.typesafe.scalalogging.LazyLogging

import ch.uzh.ifi.seal.lisa.core._
import ch.uzh.ifi.seal.lisa.core.public._
import ch.uzh.ifi.seal.lisa.core.computation.NoneDomain
import ch.uzh.ifi.seal.lisa.module.analysis.UniversalAnalysisSuite
import ch.uzh.ifi.seal.lisa.module.persistence.CSVPersistence

import ch.uzh.ifi.seal.lisa.module.analysis.MccAnalysis.Mcc
import ch.uzh.ifi.seal.lisa.module.analysis.MethodCountAnalysis.MethodCount
import ch.uzh.ifi.seal.lisa.module.analysis.MethodParameterCountAnalysis.MethodParameterCount
import ch.uzh.ifi.seal.lisa.module.analysis.VariableCountAnalysis.VariableCount
import ch.uzh.ifi.seal.lisa.module.analysis.StatementCountAnalysis.StatementCount
import ch.uzh.ifi.seal.lisa.module.analysis.NameAnalysis.Name
import ch.uzh.ifi.seal.lisa.module.analysis.UniversalAnalysisSuite
import ch.uzh.ifi.seal.lisa.core.computation.AnalysisState


abstract class LisaSpec(val id: String) extends SpecificationLike with LazyLogging {

  sequential

  import java.util.UUID
  implicit val uid = UUID.randomUUID().toString

  val config = ConfigFactory.load("lisa.conf")
  val gitLocalDir = config.getConfig("lisa").getString("git.localDir")
  val testRepoUrl = config.getConfig("lisa").getString(s"$id.git.url")
  val startCommit = Option(config.getConfig("lisa").getString(s"$id.git.startCommit")).filter(_.trim.nonEmpty)
  val endCommit = Option(config.getConfig("lisa").getString(s"$id.git.endCommit")).filter(_.trim.nonEmpty)
  val resultDir = config.getConfig("lisa").getString(s"$id.resultDir")

  val analyses: AnalysisSuite = UniversalAnalysisSuite
  val persistence: Persistence = new CSVPersistence(resultDir)

  implicit val domain: Domain = NoneDomain

  def makeComputation(parsers: List[Parser], console: Boolean = false,
                      periodicStats: Boolean = false): LisaComputation = {
    val sources = new GitAgent(parsers, testRepoUrl, gitLocalDir, startCommit, endCommit)
    new LisaComputation(sources, analyses, persistence, console = console,
                        periodicStats= periodicStats)
  }

  def makeComputationWithSources(parsers: List[Parser], sources: SourceAgent,
                                 console: Boolean = false,
                                 periodicStats: Boolean = false): LisaComputation = {
    new LisaComputation(sources, analyses, persistence, console = console,
                        periodicStats= periodicStats)
  }

  var classAggregates = List[(String,RevisionRange,Int,Int,Int,BigInt,Int)]()

  def aggregateVertex(v: BaseVertex, aggregate: ConcurrentLinkedQueue[(String,RevisionRange,Int,Int,Int,BigInt,Int)]) {
    var shouldAggregate = v.state.rangeStates.values.exists { state =>
      state is ('class, 'interface)
    }
    if (shouldAggregate) {
      v.state.rangeStates.foreach { case (range, state) =>
        val name = state[Name].name
        if (state is 'class) {
          aggregate.add((
            name, range,
            state[MethodCount].n,
            state[MethodParameterCount].n,
            state[VariableCount].n,
            state[Mcc].n,
            state[StatementCount].n))
        }
      }
    }
  }

  def getClassAggregates(graph: Graph[Any,Any]): List[(String,RevisionRange,Int,Int,Int,BigInt,Int)] = {
    val aggregate = new ConcurrentLinkedQueue[(String,RevisionRange,Int,Int,Int,BigInt,Int)]()
    graph.foreachVertex {
      case v: AstVertex => aggregateVertex(v, aggregate)
      case v: ObjectVertex => aggregateVertex(v, aggregate)
      case _ =>
    }
    aggregate.toArray(Array[(String,RevisionRange,Int,Int,Int,BigInt,Int)]()).toList
  }

  def getRevisionData(graph: Graph[Any,Any], rev: Revision): Map[String,AnalysisState] = {
    var res = Map[String,AnalysisState]()
    graph.foreachVertex {
      case v: BaseVertex =>
        val id = v.id
        v.state.rangeStates.foreach { case (range, state) =>
          synchronized {
            if (range.contains(rev)) {
              val data = state.flatData()
              // don't persist empty data or data where only the name is known
              if (data.nonEmpty && !(data.size == 1 && data.head._1.contains("Name.name"))) {
                res += (id -> state)
              }
            }
          }
        }
    }
    res
  }

  def have(check: AnalysisState => Boolean): Matcher[AnalysisState] = { s: AnalysisState =>
    (check(s), s"incorrect result in ${s}")
  }

  def checkValuesInt(
          aggregates: List[(String,RevisionRange,Int,Int,Int,BigInt,Int)],
          expectedValues: Map[String,Map[String,Int]],
          extractor: ((String, RevisionRange, Int, Int, Int, BigInt,Int)) => 
                     (String, RevisionRange, BigInt)) = {
    checkValues(aggregates, expectedValues.map{ case(s, m) =>
      (s, m.map { case (ss, mm) => (ss, BigInt(mm)) })
    }, extractor)
  }

  def checkValues(
          aggregates: List[(String,RevisionRange,Int,Int,Int,BigInt,Int)],
          expectedValues: Map[String,Map[String,BigInt]],
          extractor: ((String, RevisionRange, Int, Int, Int, BigInt,Int)) => 
                     (String, RevisionRange, BigInt)) = {
    aggregates.foreach { it =>
      val (name, range, actualValue) = extractor(it)
      val expected = expectedValues.get(name)
      expected match {
        case Some(values) =>
          val rangeValue = values.get(range.toString)
          rangeValue match {
            case Some(v) =>
              if (actualValue != v) {
                logger.error(s"$actualValue should be $v for element $name in range $range")
              }
              assert(actualValue == v, s"for element $name in range $range")
            case _ => 
              logger.error(s"missing expected range value for class " +
                              s"$name in range $range (got $actualValue)")
              failure(s"missing expected range value for class " +
                              s"$name in range $range (got $actualValue)")
          }
        case _ => failure(s"missing expected values in test for element $name")
      }
    }
  }

  def prepare(c: LisaComputation) = {
    "load the selected revisions and files from the source" in {
      c.fetch
      c.load
      success
    }

    "parse the selected files" in {
      c.parse
      success
    }
  }

  def compute(c: LisaComputation) = {
    "calculate global compression ratios" in {
      c.statCompressionRatios("ante_computation")
      success
    }
    "run the computations" in {
      c.compute
      success
    }
    "re-calculate global compression ratios" in {
      c.statCompressionRatios("post_computation")
      success
    }
  }

  def persist(c: LisaComputation) = {
    "persist the results" in {
      c.persist
      success
    }
  }

  def finalize(c: LisaComputation) = {
    "shutdown the computation and cleanup the source" in {
      c.shutdown
      c.cleanup
      success
    }
  }

  def basicTest(c: LisaComputation) = {
    prepare(c)
    compute(c)
    persist(c)
    finalize(c)
  }

}
