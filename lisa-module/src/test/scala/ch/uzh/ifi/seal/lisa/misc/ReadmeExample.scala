package ch.uzh.ifi.seal.lisa.misc

import ch.uzh.ifi.seal.lisa.core.public.LisaComputation
import ch.uzh.ifi.seal.lisa.core.source.GitAgent
import ch.uzh.ifi.seal.lisa.module.parser.{AntlrCSharpParser,NashornJavascriptParser}
import ch.uzh.ifi.seal.lisa.module.analysis.UniversalAnalysisSuite
import ch.uzh.ifi.seal.lisa.module.persistence.CSVPerRevisionParallelizedPersistence

object ReadmeExample extends App {
  // a unique identifier used for to name log files and actor systems
  implicit val uid = "lisa-jint"
  // the parser we want to use
  val parsers = List(AntlrCSharpParser, NashornJavascriptParser)
  // provide access to the sources
  val sources = new GitAgent(parsers,
    url = "https://github.com/sebastienros/jint.git",
    localDirPath = "/tmp/example",
    start = Some("1db58da6bb1b931b1cfc7d454d18e25061ae3cb7"),
    end = Some("e6ea95d427b2ed7a13112dfa2ca26d60ac71249e")
  )
  // the analyses we want to run. Multiple suites can be merged using '+'
  val analyses = UniversalAnalysisSuite
  // how we want to persist the results
  val persistence = new CSVPerRevisionParallelizedPersistence("/tmp/jint-results")
  // prepare a computation
  val c = new LisaComputation(sources, analyses, persistence)
  // run the computation
  c.execute
}

