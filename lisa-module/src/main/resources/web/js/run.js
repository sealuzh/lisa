if (!probablyCompatible()) {
  throw new Error(browserCheckMsg);
}

var subject = window.location.search.split("=")[1]
if (subject) {
  subject = subject.replace(/\./g, '').replace(/#/g, '');
  subject = `./data/${subject}.json`;

}
else {
  subject = "./api/graph.json";
}

d3.json(subject)
  .then(function (json) {
    d3.select("#warningMsg").text("Rendering...");
    setTimeout( function () { // to ensure "Rendering..." gets drawn to DOM before rendering
      const drawing = new Drawing(json);
      drawing.draw();
      d3.select("#warningMsg").text("")
      if (subject.includes("tutorial.json")) {
        const tutorial = new Tutorial(drawing);
        tutorial.run();
      }
    }, 0);
  })
  .catch(function (error) {
    d3.select("#warningMsg").html("Project not found<br/>Select the tutorial or a different project in the top left corner");
  });

