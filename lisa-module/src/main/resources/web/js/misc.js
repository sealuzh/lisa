
const deepcopy = function (aObject) {
  if (!aObject) { return aObject; }

  let v;
  let bObject = Array.isArray(aObject) ? [] : {};
  for (const k in aObject) {
    v = aObject[k];
    bObject[k] = (typeof v === "object") ? deepcopy(v) : v;
  }

  return bObject;
}

const tooltip = d3.selectAll("#tooltip")
    .style("visibility", "hidden");
function tooltipShow(infodata) {
  const infos = tooltip.selectAll(".info")
    .data(infodata);
  const info = infos
    .enter().append("div").attr("class", "info");
  info
    .append("div").attr("class", "key")
    .text(function (d) { return d[0]; })
    .style("width", function (d) { if (d[0] == "") { return 0; } else { return null; } });
  info
    .append("div").attr("class", "value")
    .text(function (d) { return String(d[1]).replace(/^\s*$(?:\r\n?|\n)/gm, ""); });
  tooltip
    .style("visibility", "visible");
}
function tooltipMove() {
  const offset = 20;
  const yPos = Math.max(
    Math.min(
      d3.event.pageY + offset,
      height-tooltip.node().getBoundingClientRect().height - offset
    ), offset)
  tooltip
    .style("left", `${d3.event.pageX + offset}px`)
    .style("top", `${yPos}px`);
}
function tooltipHide() {
  tooltip
    .style("visibility", "hidden")
    .selectAll("*").remove();
}

