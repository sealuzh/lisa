function Db (json) {
  const self = this;

  // load data from json and pre-process where necessary
  this.revisions = json["revisions"];

  // author names and sorting
  const authorCommitCounts = self.revisions.reduce(function (acc, a) {
    if (!acc.hasOwnProperty(a.authorName)) { acc[a.authorName] = 0; }
    acc[a.authorName] += 1;
    return acc;
  }, {})
  this.authorRanks = Object.keys(authorCommitCounts)
    .sort(function(a,b){ return authorCommitCounts[a] - authorCommitCounts[b]; })
    .reduce(function (acc, a, i) { acc[a] = i; return acc; }, {})
  this.authorSorting = function (a, b) { return self.authorRanks[b] - self.authorRanks[a]; };
  this.authorsSelected = Array.from(new Set([...self.revisions].map(function (r) {
    return r.authorName;
  }))).sort(self.authorSorting);

  // determine the largest nodes to build the size scale
  this.classes = json["classes"]
  this.nodeSizeMax = Math.max(...self.classes.map(function (c) {
    return Math.max(...c.ranges.map(function (r) { return r.metrics["size"]}));
  }));
  this.nodeSizeScale = d3.scaleLog().domain([1, self.nodeSizeMax]).range([25,100]);

  // pre-process classes
  this.classes = Object.entries(this.classes.map(rangeFill).map(function (d) {
    const group = d.id.replace(/[^/]*$/, '').slice(0, -1);
    d.rangesAll = deepcopy(d.ranges);
    d.group = group;
    if (d.hasOwnProperty("methods")) {
      d.methods.forEach(function (m) {
        m.owner = d;
        m.group = group;
        m.isMethod = true;
        m.rangesAll = deepcopy(m.ranges);
      })
    }
    d.nodeSize = nodeSize(d);
    d.methods.forEach(function (m) { m.nodeSize = nodeSize(m); });
    return d;
  }).reduce(function (acc, d) {
    const rangesHash = d.rangesAll.reduce(function (hash, r) {
      if (!r.rev) { return hash; }
      return hash + r.rev.slice(0,8);
    }, []);
    if (!acc.hasOwnProperty(rangesHash)) { acc[rangesHash] = []; }
    acc[rangesHash].push(d)
    return acc
  }, {})).reduce(function (acc, pair) {
    const [key, nodes] = pair;
    const sorted = nodes.sort(function (a, b) { return a.nodeSize - b.nodeSize; });
    const leader = nodes.pop();
    nodes.forEach(function (c) {
      c.coevolvedShown = false;
      c.coevolvedWith = leader;
    })
    leader.coevolved = nodes;
    acc.push(leader);
    return acc;
  }, []);

  this.classesHidden = [];
  this.classesSelected = this.classes;
  this.inheritance = json["inheritance"].concat(json["implements"]);
  this.selectLinks = function (d) {
    const sourceExists = self.classesSelected.find(function (c) { return c.id == d.source || c.id == d.source.id });
    const targetExists = self.classesSelected.find(function (c) { return c.id == d.target || c.id == d.target.id });
    return sourceExists && targetExists;
  };
  this.inheritanceSelected = this.inheritance.filter(self.selectLinks);

  this.calls = json["calls"];
  function callCounting (acc, d) {
    const edgeId = `${d.source.id}->${d.target.id}`
    if (!acc.hasOwnProperty(edgeId)) { d.count = 1; acc[edgeId] = d; }
    acc[edgeId].count += 1;
  }
  this.selectCalls = function (d) {
    self.callsSelected = Object.entries(this.calls.reduce(function (acc, d) {
      const c1 = self.classesSelected.find(function (c) { return c.id == d.c1 });
      const c2 = self.classesSelected.find(function (c) { return c.id == d.c2 });
      const m1 = self.classesSelected.find(function (c) { return c.id == d.m1 });
      const m2 = self.classesSelected.find(function (c) { return c.id == d.m2 });
      if (c1 && c2) {
        if (!m1 && !m2) {
          callCounting(acc, {"d": d, "source": c1, "target": c2, "relation": "CALLS"})
        }
        else if (m1 && m2) {
          callCounting(acc, {"d": d, "source": m1, "target": m2, "relation": "CALLS"})
        }
        else if (!m1 && m2) {
          callCounting(acc, {"d": d, "source": c1, "target": m2, "relation": "CALLS"})
        }
        else if (m1 && !m2) {
          callCounting(acc, {"d": d, "source": m1, "target": c2, "relation": "CALLS"})
        }
      }
      return acc;
    }, {})).reduce(function (acc, pair) {
      const [key, value] = pair;
      acc.push(value)
      return acc;
    }, []);
  }

  self.selectCalls()

  this.selectedRange = [0,this.revisions.length-1];
  this.allPackages = this.classes.reduce(function (acc, d) {
    acc.add(d.group);
    if (d.coevolved) { d.coevolved.forEach(function (c) { acc.add(c.group); }) }
    return acc;
  }, new Set());
  this.selectedPackages = new Set();

  this.nodeIdFilter = "";
  this.selectRange = function () {
    d3.select("#warningMsg").text("");
    const start = self.selectedRange[0];
    const end = self.selectedRange[1];
    const authorSelection = new Set();
    self.classesSelected = self.classes.reduce(function (acc, c) {
      if (self.nodeIdFilter != "" && !c.id.includes(self.nodeIdFilter)) { return acc; }
      if (c.isMethod && c.owner.coevolvedShown == false) { return acc; }
      if (self.selectedPackages.size > 0 && !self.selectedPackages.has(c.group)) { return acc; }
      c.ranges = rangeCrop(c.rangesAll, start, end)
      if (c.ranges.length == 0) return acc;
      const allEmpty = c.ranges.reduce(function (acc, d) {
        if (d.metrics.size != 0) {
          authorSelection.add(self.revisions[d.startDisplay].authorName)
          return false;
        }
        else return acc;
        }, true)
      if (allEmpty) return acc;
      acc.push(c);
      return acc;
    }, [])
    if (self.classesSelected.length == 0) {
      d3.select("#warningMsg").text("No nodes with applied filters are within range");
    }
    self.selectCalls();
    self.inheritanceSelected = self.inheritance.filter(self.selectLinks)
    self.authorsSelected = Array.from(authorSelection).sort(self.authorSorting);
  }

  // computes an average size for a given node based on the current scale
  function nodeSize(d) {
    var exists = 0;
    const totalSize = d.rangesAll.reduce( function (acc, r)  {
      const size = parseInt(r.metrics.size);
      if (size > 0) { exists += 1; }
      return acc + size;
    }, 0);
    const nodeSize = totalSize / exists;
    return self.nodeSizeScale(nodeSize);
  }

  // fills gaps in evolution with empty data for drawing gaps in pies
  function rangeFill(node) {
    const last = node.ranges.length - 1;
    node.ranges = node.ranges.reduce(function (acc, r, i) {
      if (i == 0) {
        if (r.start != 0) {
          const buffer = {"start": 0, "end": r.start - 1, "metrics": {"size": 0}, "rev": self.revisions[0].rev};
          buffer.startDisplay = buffer.start;
          buffer.endDisplay = buffer.end;
          acc.push(buffer);
        }
      }
      else if (acc[acc.length -1].end + 1 != r.start) {
        const buffer = {"start": acc[acc.length -1].end + 1, "end": r.start - 1, "metrics": { "size": 0}, "rev": self.revisions[acc[acc.length -1].start].rev};
        buffer.startDisplay = buffer.start;
        buffer.endDisplay = buffer.end;
        acc.push(buffer)
      }
      r.startDisplay = r.start;
      r.endDisplay = r.end;
      acc.push(r)
      if (i == last && r.end != self.revisions.length - 1) {
        const buffer = {"start": r.end + 1, "end": self.revisions.length - 1, "metrics": {"size": 0}, "rev": self.revisions[r.end + 1].rev};
        buffer.startDisplay = buffer.start;
        buffer.endDisplay = buffer.end;
        acc.push(buffer);
      }
      return acc;
    }, []);
    if (node.hasOwnProperty("methods")) { node.methods = node.methods.map(rangeFill); }
    return node;
  }

  function rangeCrop(ranges, start, end) {
    return ranges.reduce(function (acr, r) {
      if (r.start >= start && r.end <= end) {
        acr.push(r);
      }
      else if (r.end < start || r.start > end) {}
      else if (r.start < start && r.end >= start) {
        r = deepcopy(r);
        r.start = start;
        r.rev = self.revisions[start];
        acr.push(r);
      }
      else if (r.end > end && r.start <= end) {
        r = deepcopy(r);
        r.end = end;
        acr.push(r);
      }
      return acr;
    }, []);
  }

}

