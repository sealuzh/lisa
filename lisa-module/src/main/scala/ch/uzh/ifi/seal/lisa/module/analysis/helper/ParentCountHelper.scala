package ch.uzh.ifi.seal.lisa.module.analysis

import scala.reflect.{ClassTag, classTag}
import ch.uzh.ifi.seal.lisa.core._
import ParentCountAnalysis.ParentCount

// TODO: is this really necessary?
trait ParentCountHelper {
  def root(state: AnalysisState): Boolean = { state[ParentCount].n == 0 }
  def allParents[D <: Data : ClassTag](input: AnalysisState)(
      incomplete: => AnalysisState,
      complete: => AnalysisState): AnalysisState = {
    val id = classTag[D]
    val c = input[ParentCount]
    val ac = c.counters.getOrElse(id, 0) + 1
    (if (ac < c.n) incomplete else complete) + c.copy(counters = c.counters + (id -> ac))
  }
}


