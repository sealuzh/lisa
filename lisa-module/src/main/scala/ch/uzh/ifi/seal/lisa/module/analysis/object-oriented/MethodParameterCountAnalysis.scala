package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

/** Counts the number of parameters of methods and their sum per class
  *
  * The analysis starts on VARIABLE vertices which are parameters and increments
  * a counter when reaching methods, files, or a class-like vertices.
  */
object MethodParameterCountAnalysis extends Analysis {
  case class MethodParameterCount(persist: Boolean, n: Int) extends Data {
    def this() = this(false, 0)
  }

  override def start = { implicit domain => state =>
    if (state.is('parameter)) {
        state ! new MethodParameterCountPacket(1)
    } else if (state.is('method) && state[MethodParameterCount].n != 0) {
        state ! new MethodParameterCountPacket(state[MethodParameterCount].n)
    }
    else { state }
  }

  class MethodParameterCountPacket(val count: Int) extends AnalysisPacket {
    override def collect = { implicit domain => state =>
      if (state.is('class, 'file, 'interface, 'method)) {
        // increase the local method parameter count by the incoming count
        val n = MethodParameterCount(true, state[MethodParameterCount].n + count)
        if (state.is('method)) {
          state + n ! this
        }
        else {
          state + n
        }
      }
      else { state ! this }
    }
  }

}
