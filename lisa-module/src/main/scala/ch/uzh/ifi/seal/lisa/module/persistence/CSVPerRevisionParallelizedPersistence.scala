package ch.uzh.ifi.seal.lisa.module.persistence

import java.io.File
import scala.collection.mutable.{Map => MMap}
import ch.uzh.ifi.seal.lisa.core._
import com.github.tototoshi.csv._
import com.typesafe.scalalogging.LazyLogging

import ch.uzh.ifi.seal.lisa.core.public._

class CSVPerRevisionParallelizedPersistence(resultDir: String)(implicit uid: String) extends Persistence with LazyLogging with CSVStats {
  import com.signalcollect.interfaces.ModularAggregationOperation
  import com.signalcollect.Vertex
  class GlobalAggregation extends ModularAggregationOperation[Map[Int,Map[String,Long]]] {
    val neutralElement = Map[Int,Map[String,Long]]()
    def aggregate(a: Map[Int,Map[String,Long]], b: Map[Int,Map[String,Long]]): Map[Int,Map[String,Long]] = {
      b.foldLeft(a) { case (a, (k, v)) =>
        a.get(k) match {
          case Some(aData) => a + (k -> v.foldLeft(aData) { case (aa, (kk, vv)) =>
            aa.get(kk) match {
              case Some(aValue) => aa + (kk -> (aValue + vv))
              case _ => aa + (kk -> vv)
            }})
          case _ => a + (k -> v)
        }
      }
    }
    def extract(x: Vertex[_, _, _, _]): Map[Int,Map[String,Long]] = {
      x match {
        case v: BaseVertex => {
          v.state.rangeStates.foldLeft(Map[Int,Map[String,Long]]()) { case (acc, (range, state)) =>
            implicit val domain = v.domain
            if (state is 'file) {
              // retrieve data map from state
              val data = state.flatData().collect {
                case (k, v) if v.isInstanceOf[Long] => (k, v.asInstanceOf[Long])
                case (k, v) if v.isInstanceOf[Int] => (k, v.asInstanceOf[Int].toLong)
              }
              (range.start.n to range.end.n).foldLeft(acc) { case (acc, n) =>
                acc + (n -> data)
              }
            } else acc
          }
        }
        case _ => neutralElement
      }
    }
  }

  val target = if (resultDir endsWith "/") resultDir else resultDir + "/"

  override def persist(c: LisaComputation) {
    val timestamp = System.nanoTime()
    logger.info(s"persisting results to $target ...")
    val tempFile = s"${target}global.csv.temp"
    val globalFile = s"${target}global.csv"
    (new File(target)).mkdirs

    val global = c.graph.aggregate(new GlobalAggregation())
    var headers = MMap[String,Int]()

    var writer = CSVWriter.open(new File(tempFile))
    c.sources.getRevisions.get.foreach { case (n, rev) =>
      val data = global.getOrElse(n, Map[String,Long]())
      var dataCells = Map[Int, Long]()
      data.foreach { case (k, v) =>
        val cellIndex = headers.getOrElseUpdate(k, headers.size)
        dataCells = dataCells + (cellIndex -> v)
      }
      val dataRow = (0 to headers.size-1) map { i =>
        dataCells.getOrElse(i, 0).toString
      }
      writer.writeRow(List[String](
        rev.n.toString,
        rev.rev,
        rev.authorName,
        rev.authorEmail,
        rev.getAuthorDateString(),
        rev.committerName,
        rev.committerEmail,
        rev.getCommitterDateString()
      ) ++ dataRow)
    }
    writer.close

    // writer headers to final file
    val headersByIndex = headers.map(_.swap)
    writer = CSVWriter.open(new File(globalFile))
    writer.writeRow(List(
      "N",
      "Hash",
      "AuthorName",
      "AuthorEmail",
      "AuthorDate",
      "CommitterName",
      "CommitterEmail",
      "CommitterDate"
    ) ++ ((0 to headers.size-1) map { i =>
      val h = headersByIndex(i)
      //val short = h.dropWhile(_ != '$').drop(1)
      val short = h.split("(\\.|\\$)").reverse.take(2).reverse.mkString(".")
      if (short.isEmpty) h else short
    }))
    writer.close

    // append temp file contents to final file
    import java.io.FileInputStream
    import java.io.FileOutputStream
    val in = new FileInputStream(tempFile)
    val out = new FileOutputStream(globalFile, true)
    var ch = 0
    while ({ch = in.read; ch != -1}) { out.write(ch) }
    in.close
    out.close
    (new File(tempFile)).delete

    val duration = (System.nanoTime - timestamp)/1000000000.0
    stats += ("persist_seconds" -> duration)
    storeStats(s"${target}stats.csv")

    logger.info(f"done persisting; duration: ${duration}%3.2f")
  }
}


