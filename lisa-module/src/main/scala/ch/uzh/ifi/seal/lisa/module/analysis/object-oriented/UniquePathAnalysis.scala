package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

  /** An analysis which computes the number of unique paths through a method
    *
    * This is an extension of McCabe's complexity, in that the branching points
    * are not only counted but used to compute through how many different paths
    * the control can flow. branching points such as if/case/while/for normally
    * increase this value by 1, while blocks (e.g. method bodies) multiply all
    * incoming values, leading to a quick explosion of the possible number of
    * permutations
    */
object UniquePathsAnalysis extends Analysis with ChildCountHelper {
  case class UniquePaths(persist: Boolean, n: BigInt) extends Data {
    def this() = this(false, 1)
  }

  override def start = { implicit domain => state =>
    // start on any leaf vertex
    if (leaf(state)) state ! new UniquePathPacket(if (state is 'fork) 2 else 1)
    else state
  }

  class UniquePathPacket(val count: BigInt) extends AnalysisPacket {
    override def collect = { implicit domain => state =>
      val d = state[UniquePaths]

      val oldUniquePath = d.n
      val newUniquePath = if (state.is('block)) oldUniquePath * count else oldUniquePath + count - 1
      val persist = state is ('class, 'method, 'file)
      val newData = UniquePaths(persist, newUniquePath)

      allChildren[UniquePaths](state)(
        incomplete = state + newData,
        complete = {
          val newCount = if (state is 'fork) newUniquePath + 1 else newUniquePath
          state + newData ! new UniquePathPacket(newCount)
        }
      )
    }
  }
}
