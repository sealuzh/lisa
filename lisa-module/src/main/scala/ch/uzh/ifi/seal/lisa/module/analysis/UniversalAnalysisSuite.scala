package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

/** An analysis which is passed around the graph during a computation
  *
  * Analayses are split into starting logic and an execution logic. Every time
  * a vertex is created, analyses have a chance to check wether they want to use
  * the vertex as a starting point for an analysis. If yes, a new Analysis will
  * be created with some data and sent along the outgoing edges upon signalling.
  * When the destination vertex receives the signal, any Analysis instances
  * contained in the signal can process the local data and modify the state.
  * They can also create new Analysis instances or forward themselves to
  * propagate through the graph.
  *
  * All of the range handling (one vertex having different states for different
  * revision ranges) is abstracted so that individual Analysis implementations
  * are only concerned with a single state.
  *
  * Refer to the documentation of the functions of the Analysis class and its
  * companion object as well as the State documentation to understand how an
  * analysis is performed
  */
object UniversalAnalysisSuite extends AnalysisSuite {
  /** Definition of computation phases, which are executed sequentially
    *
    * Because some analyses depend on data created by other analyses, the entire
    * computation is split into phases. Analyses with no dependencies get run
    * first. Analyses run during later phases can use data of analyses of
    * earlier phases.
    */
  val phases: List[List[Analysis]] = List(
    List(
      ChildCountAnalysis,
      AntlrJavascriptNameAnalysis,
      AntlrJavascriptAnonFunNameAnalysis,
      AntlrCSharpNameAnalysis,
      AntlrJavaNameAnalysis,
      AntlrPython3NameAnalysis,
      PythonNativeNameAnalysis,
      JDKJavaNameAnalysis
    ),
    List(
      VertexCountAnalysis,
      MccAnalysis,
      UniquePathsAnalysis,
      ControlNestingAnalysis,
      VariableCountAnalysis,
      MethodCountAnalysis,
      MethodParameterCountAnalysis,
      StatementCountAnalysis,
      ClassCountAnalysis
    ),
    List(
      BrainMethodAnalysis
    )
  )
}

