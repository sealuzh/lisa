package ch.uzh.ifi.seal.lisa.module.analysis

import scala.reflect.ClassTag
import ch.uzh.ifi.seal.lisa.core._

object ParentCountAnalysis extends Analysis {
  /** An analysis which counts the number of children of a vertex
    *
    * The analysis starts on any vertex is sent along a single edge and then
    * collected immediately.
    */
  case class ParentCount(persist: Boolean = false, n: Int = 0, counters: Map[ClassTag[_], Int]) extends Data {
    def this() = this(false, 0, Map())
  }

  override def start = { _ => state =>
    state ↓ new ParentCountPacket(1)
  }

  class ParentCountPacket(val count: Int) extends AnalysisPacket {
    override def collect = { _ => state =>
      val n = ParentCount(false, state[ParentCount].n + count, Map())
      state + n
    }
  }
}

