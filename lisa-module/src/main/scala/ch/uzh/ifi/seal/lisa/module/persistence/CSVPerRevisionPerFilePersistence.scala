package ch.uzh.ifi.seal.lisa.module.persistence

import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.File
import collection.JavaConverters._
import ch.uzh.ifi.seal.lisa.core._
import com.github.tototoshi.csv._
import com.typesafe.scalalogging.LazyLogging

import ch.uzh.ifi.seal.lisa.core.public._

class CSVPerRevisionPerFilePersistence(resultDir: String) extends Persistence with LazyLogging {

  val target = if (resultDir endsWith "/") resultDir else resultDir + "/"

  override def persist(c: LisaComputation) {
    logger.info(s"persisting results to $target ...")
    // create a directory for each revision
    c.sources.getRevisions.get.map { case (n, r) =>
      val targetSubdir = s"${target}/${r.n}"
      (new File(targetSubdir)).mkdirs
    }
    // keep track of which data goes in which column
    val columns = new java.util.concurrent.ConcurrentHashMap[String, Int]().asScala
    // create necessary writers on the fly
    val writers = new java.util.concurrent.ConcurrentHashMap[(Int, String), CSVWriter]().asScala
    c.computation.graph.foreachVertex {
      case v: BaseVertex =>
        val id = v.id
        v.state.rangeStates.foreach { case (range, state) =>
          // retrieve data map from state
          val data = state.flatData()
          if (data.nonEmpty) {
            var rowElements = Map[Int, String]()
            data.foreach { case (header, value) =>
              // determine column where this field should go
              val column = columns.getOrElse(header, {
                val newColumn = columns.size
                  columns += (header -> newColumn)
                  newColumn
                }
              )
              rowElements = rowElements + (column -> value.toString)
            }
            val row = (0 to columns.size - 1).foldLeft(List[String](v.id, range.toString, range.toCommitsString)) { case (acc, index) =>
              val cell = rowElements.getOrElse(index, "")
              acc :+ cell
            }
            synchronized {
              val vertexFile = v.id.drop(1).replaceAll("/","-")
              for (n <- range.start.n to range.end.n) {
                val writerId = (n, vertexFile)
                val writer = writers.get(writerId)
                writer match {
                  case Some(w) => w.writeRow(row)
                  case None => {
                    val fileName = s"${target}/${n}/${vertexFile}-temp.csv"
                    val writer = CSVWriter.open(new File(fileName))
                    writer.writeRow(row)
                    writers += (writerId -> writer)
                  }
                }
              }

            }
          }
        }
      case _ =>
    }
    writers.values.foreach { w =>
      w.close
    }

    val headerRow = List("ASTPath", "RevisionRangeNumerical", "RevisionRange") ++ columns.toList.sortBy(_._2).map(_._1)
    writers.foreach { case ((n, fn), w) =>
      val r = c.sources.getRevisions.get.get(n).get
      val tempFile = s"${target}/${r.n}/${fn}-temp.csv"
      val outFile = s"${target}/${r.n}/${fn}.csv"
      val writer = CSVWriter.open(new File(outFile))
      writer.writeRow(headerRow)
      writer.close()
      val in = new FileInputStream(tempFile)
      val out = new FileOutputStream(outFile, true)
      var ch = 0
      while ({ch = in.read; ch != -1}) {
        out.write(ch)
      }
      in.close
      (new File(tempFile)).delete
      out.close
    }
    logger.info("done persisting results!")
  }
}


