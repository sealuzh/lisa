package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

/** An analysis which counts the number of variables in a class */
object VariableCountAnalysis extends Analysis {
  case class VariableCount(persist: Boolean, n: Int) extends Data {
    def this() = this(false, 0)
  }

  override def start = { implicit domain => state =>
    if (state.is('variable, 'field, 'parameter)) state ! new VariableCountPacket(1) else state
  }

  class VariableCountPacket(val count: Int) extends AnalysisPacket {
    override def collect = { implicit domain => state =>
      if (state.is('class, 'file, 'interface, 'method)) {
        // increase the local variable count by the incoming count
        val n = VariableCount(true, state[VariableCount].n + count)
        state + n ! this
      }
      else { state ! this }
    }
  }
}
