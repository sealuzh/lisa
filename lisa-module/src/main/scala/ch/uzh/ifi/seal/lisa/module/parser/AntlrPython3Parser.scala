package ch.uzh.ifi.seal.lisa.module.parser

import ch.uzh.ifi.seal.lisa.antlr._
import ch.uzh.ifi.seal.lisa.antlr.parser.Python3Lexer
import ch.uzh.ifi.seal.lisa.antlr.parser.Python3Parser

object AntlrPython3ParseTree extends Domain {
  override val mapping = Map(
    'class     -> Set("Classdef"),
    'block     -> Set("Suite"),
    'statement -> Set("Expr_stmt",
                      "Del_stmt",
                      "Pass_stmt",
                      "Break_stmt",
                      "Continue_stmt",
                      "Return_stmt",
                      "Raise_stmt",
                      "Yield_stmt",
                      "Import_stmt",
                      "Global_stmt",
                      "Nonlocal_stmt",
                      "Assert_stmt",
                      "If_stmt",
                      "While_stmt",
                      "For_stmt",
                      "Try_stmt",
                      "With_stmt"),
    'fork      -> Set("If_stmt",
                      "While_stmt",
                      "For_stmt",
                      "Try_stmt",
                      "With_stmt",
                      "Conditional_test"),
    'method    -> Set("Funcdef"),
    'variable  -> Set("Assign"),
    'parameter -> Set("Vfpdef", "Tfpdef")
  )
}

object AntlrPython3Parser extends AntlrParser[Python3Parser](AntlrPython3ParseTree) {
  override val suffixes = List(".py")
  override def lex(input: ANTLRInputStream) = new Python3Lexer(input)
  override def parse(tokens: CommonTokenStream) = new Python3Parser(tokens)
  override def enter(parser: Python3Parser) = parser.file_input()
}

