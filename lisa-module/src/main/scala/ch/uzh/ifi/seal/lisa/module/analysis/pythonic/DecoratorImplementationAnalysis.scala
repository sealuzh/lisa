package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

/** Detects the implementation of a "traditional" decorator
  *
  * Note that this analysis can only detect decorator which follow the following
  * "traditional" pattern regarding the semantics of decorators: A decorator
  * consists of a function D which takes another function F as a parameter (F is
  * the function to be decorated). It then defines a nested function N, which in
  * turn calls F and returns said function. The pattern, in short, is:
  *   def D(F): def N(...): return do_stuff(F(_)); return N
  * This means that we need to collect:
  *  - A: The name of the argument (argargs -> Name)
  *  - B: The name of the final return value (ReturnBody -> Name)
  *  - C: The name of the function called inside the nested function (Call -> Name)
  *  - D: The name of the inner function (FunctionDefbody -> Name)
  * If, in the collected data, A == D and B == C, then we found a decorator
  *
  * Example nodes:
  * /Module0/ FunctionDefbody0 - FunctionDefbody, {'str': 'D'}
  * /Module0/FunctionDefbody0/arguments1/ argargs0 - argargs, {'str': 'F'}
  * /Module0/FunctionDefbody0/ FunctionDefbody0 - FunctionDefbody, {'str': 'N'}
  * /Module0/FunctionDefbody0/FunctionDefbody0/Returnbody1/ Call0 - Call, {}
  * /Module0/FunctionDefbody0/FunctionDefbody0/Returnbody1/Call0/ Name0 - Name, {'str': 'F'}
  * /Module0/FunctionDefbody0/ Returnbody1 - Returnbody, {}
  * /Module0/FunctionDefbody0/Returnbody1/ Name0 - Name, {'str': 'N'}
  *
  * Example AST:
  * Module
  *   -> FunctionDefbody {str: D}
  *     -> arguments
  *       -> argargs {str: F}
  *     -> FunctionDefbody {str: N}
  *       -> Returnbody
  *         -> Call
  *           -> Name {str: F}
  *     -> Returnbody
  *       -> Name {str: N}
  */
object DecoratorImplementationAnalysis extends Analysis with ChildCountHelper {

  case class DecoratorImplementation(persist: Set[String], val argargs: Option[String],
      funDefbody: Option[String], funDefName: Option[String],
      retName: Option[String], n: Int) extends Data {
    def this() = this(Set[String](), None, None, None, None, 0)
    def populated = (argargs.nonEmpty && funDefbody.nonEmpty &&
                     funDefName.nonEmpty && retName.nonEmpty)
  }

  override def start = { implicit domain => state =>
    // the analysis starts on Name or argargs nodes
    if (state is ("Name", "argargs")) {
      state[Literal].map.get("str") match {
        case Some(n) => {
          if (state is "Name") {
            // the current node could be the call name inside the inner function
            // or the return value name returned by the decorator
            state ! new DecoratorImplementationPacket(funDefName = Some(n), retName = Some(n))
          }
          else {
            // the current node is the argargs of the decorator function
            state ! new DecoratorImplementationPacket(argargs = Some(n))
          }
        }
        case _ => state
      }
    } else state
  }

  class DecoratorImplementationPacket(val argargs: Option[String] = None,
      funDefbody: Option[String] = None, funDefName: Option[String] = None,
      retName: Option[String] = None, n: Int = 0) extends AnalysisPacket {

    override def collect = { implicit domain => state =>

      implicit val s = state

      val old = state[DecoratorImplementation]
      val persist = if (state is ('class, 'method, 'file)) Set("n") else Set[String]()

      // If n > 0 the package is a simple counter. Store it and pass it along
      val newState = if (n > 0) {
        val newN = old.n + n
        state + old.copy(persist = persist, n = newN) ! new DecoratorImplementationPacket(n = newN)
      }
      // Otherwise, propagate data from lower levels appropriately
      else if (argargs.nonEmpty && (state is "arguments")) {
        // the current node is the arguments node between argargs and the decorator function
        state ! this
      }
      else if (funDefbody.isEmpty && funDefName.nonEmpty && retName.nonEmpty && (state is "Call")) {
        // the current node is the Call node inside the nested function
        state ! new DecoratorImplementationPacket(funDefName = funDefName)
      }
      else if (funDefbody.isEmpty && funDefName.nonEmpty && retName.nonEmpty && (state is "Returnbody")) {
        // the current node is the Returnbody returning the inner function
        state ! new DecoratorImplementationPacket(retName = retName)
      }
      else if (funDefbody.isEmpty && funDefName.nonEmpty && retName.isEmpty && (state is "Returnbody")) {
        // the current node is the Returnbody inside the nested function
        state ! this
      }
      else if (funDefbody.isEmpty && funDefName.nonEmpty && retName.isEmpty && (state is "FunctionDefbody")) {
        // the current node is the inner function
        state[Literal].map.get("str") match {
          case Some(s) => state ! new DecoratorImplementationPacket(funDefbody = Some(s), funDefName = funDefName)
          case _ => state
        }
      }
      else if (funDefbody.nonEmpty && funDefName.nonEmpty && retName.isEmpty && (state is "FunctionDefbody")) {
        // the current node is the decorator function itself and we store its name
        state[Literal].map.get("str") match {
          case Some(s) => state + old.copy(funDefbody = funDefbody, funDefName = funDefName)
          case _ => state
        }
      }
      else if (argargs.nonEmpty && funDefbody.isEmpty && funDefName.isEmpty && retName.isEmpty && (state is "FunctionDefbody")) {
        // the current node is the decorator function itself and we just store the incoming argargs Name
        state + old.copy(argargs = argargs)
      }
      else if (argargs.isEmpty && funDefbody.isEmpty && funDefName.isEmpty && retName.nonEmpty && (state is "FunctionDefbody")) {
        // the current node is the decorator function itself and we just store the incoming return value Name
        state + old.copy(retName = retName)
      }
      else {
        // the current node is unrelated, no need to further process
        state ! this
      }

      val update = newState[DecoratorImplementation]

      if (update.populated) {
        // if all decorator-related data has been collected, check if the names
        // correlate like in an actual decorator and store/pass the result
        val decorator = if (update.argargs == update.funDefName &&
                            update.funDefbody == update.retName) 1 else 0
        val reset = DecoratorImplementation(persist, None, None, None, None, decorator)
        newState + reset ! new DecoratorImplementationPacket(None, None, None, None, decorator)
      } else newState
    }
  }
}

