package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

object VertexCountAnalysis extends Analysis with ChildCountHelper {
  /** An analysis which computes the number of vertices below this vertex
    *
    * The analysis keeps track of how many signals it has received from children
    * and only spawns a new analysis once all child values have been received.
    */
  case class VertexCount(persist: Boolean, n: Int) extends Data {
    def this() = this(false, 1)
  }

  override def start = { _ => state =>
    // start on any leaf vertex
    if (leaf(state)) state ! new VertexCountPacket(1) else state
  }

  class VertexCountPacket(val count: Int) extends AnalysisPacket {
    override def collect = { implicit domain => state =>

      val persist = state is ('class, 'interface, 'method, 'file)
      val vertexCount = state[VertexCount].n + count
      val newState = state + VertexCount(persist, vertexCount)

      allChildren[VertexCount](state)(
        incomplete = newState,
        complete = newState ! new VertexCountPacket(vertexCount)
      )
    }
  }
}

