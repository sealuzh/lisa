package ch.uzh.ifi.seal.lisa.module.parser

import java.nio.ByteBuffer
import com.typesafe.scalalogging.LazyLogging
import com.signalcollect.GraphEditor
import ch.uzh.ifi.seal.lisa.core._
import ch.uzh.ifi.seal.lisa.core.computation.Revision
import ch.uzh.ifi.seal.lisa.core.computation.FileMeta
import ch.uzh.ifi.seal.lisa.core.public.Parser
import ch.uzh.ifi.seal.lisa.module.analysis.NameAnalysis.Name
import ch.uzh.ifi.seal.lisa.module.analysis.MethodParameterCountAnalysis.MethodParameterCount

import jdk.nashorn.internal.parser.{ Parser => NashornParser }
import jdk.nashorn.internal.runtime.{ Source => NashornSource }
import jdk.nashorn.internal.runtime.ScriptEnvironment
import jdk.nashorn.internal.runtime.options.Options
import java.io.PrintWriter
import jdk.nashorn.internal.runtime.Context
import jdk.nashorn.internal.ir.visitor.NodeVisitor
import jdk.nashorn.internal.ir.LexicalContext
import jdk.nashorn.internal.ir.Node
import jdk.nashorn.internal.ir.FunctionNode

object NashornJavascriptParseTree extends Domain {
  override val mapping = Map(
    'class      -> Set("Script"),
    'block      -> Set("Block"),
    'statement  -> Set("ExpressionStatement",
                       "VarNode"),
    'ifStat     -> Set("IfNode"),
    'whileStat  -> Set("WhileNode"),
    'forStat    -> Set("ForNode"),
    'fork       -> Set("IfNode",
                       "WhileStatement",
                       "ForNode",
                       "CaseNode",
                       "TernaryNode"),
    'method     -> Set("FunctionNode"),
    'variable   -> Set("VarNode"),
    'identifier -> Set("IdentNode")
  )
}



/* Parser for parsing python 2/3 code using python's own native parser
 *
 * This object maintains a pool of parsers, one for each file name. If a new
 * filename is encountered, a parser is added to the pool. The code for each
 * file is then sent to the parser through a socket. The python parser then
 * uses a SocketGraphEditor to add the resulting nodes to the graph.
 */
object NashornJavascriptParser extends Parser with LazyLogging {
  override val suffixes = List(".js")
  override val excludes = List(".min.js")
  val domain = NashornJavascriptParseTree
  val werr = new PrintWriter(System.err, true)
  val wout = new PrintWriter(System.out, true)
  val options = new Options("nashorn", werr)
  def env = new ScriptEnvironment(options, wout, werr)

  override def parse(files: List[(String,ByteBuffer)],
                     graph: GraphEditor[Any, Any],
                     rev: Revision): ParseStats = {

    var totalLines: Long = 0
    val stats = files.foldLeft((0,0,0)){ case (acc, (blobname, codeBytes)) =>
      val code = bytesToChars(codeBytes)
      val revisionRange = RevisionRange(rev, rev)
      val filename = "/" + blobname

      def source = NashornSource.sourceFor(blobname, code.array)
      def parser = new NashornParser(env, source, new Context.ThrowErrorManager())
      val (tree, lineCount) = try {
        val t = parser.parse
        val lc = Option(t.getBody.getLastStatement) match {
          case Some(s) => s.getLineNumber
          case None => 0
        }
        (t, lc)
      } catch {
        case e: Throwable => return ParseStats(0, 0, 0)
      }

      val vertex = new AstVertex(filename, 0, revisionRange, filename, NashornJavascriptParseTree)
      val state = vertex.state.rangeStates(revisionRange)
      vertex.state = vertex.state.copy(
        rangeStates = vertex.state.rangeStates + (revisionRange -> (state +
              (TypeLabel(false, "META-FILE")) +
              (FileMeta(true, lineCount, code.length)))))
      graph.addVertex(vertex)

      val visitor = new UniversalNodeVisitor(graph, filename, revisionRange)
      val root = tree.accept(visitor)
      val res = visitor.getStats
      (acc._1 + lineCount, acc._2 + res._1, acc._3 + res._2)
    }
    ParseStats(stats._1, stats._2, stats._3)
  }

}

class UniversalNodeVisitor(graph: GraphEditor[Any,Any], fileName: String, revisionRange: RevisionRange) extends NodeVisitor[LexicalContext](new LexicalContext) {
  var parentUri = fileName
  val chainAST = scala.collection.mutable.Stack[String](parentUri)
  var chainGraph = scala.collection.mutable.Stack[String](parentUri)
  val forksAST = scala.collection.mutable.Map[String, Int]()
  val forksGraph = scala.collection.mutable.Map[String, Int]()
  var addCount = 0
  var ignoreCount = 0
  val domain = NashornJavascriptParseTree

  // graphDepth represents the traversal depth of the *filtered* graph. The
  // depth of the AST is retrieved from ctx.getRuleContext.depth
  var depthGraph = 0

  def getStats = { (addCount, ignoreCount) }

  def noChanges(state: AnalysisState): AnalysisState = state

  def enterGeneric(node: Node, alias: Option[String] = None,
                   postprocess: AnalysisState => AnalysisState = noChanges) {
    val ctx = getLexicalContext
    val name = alias.getOrElse(node.getClass.getSimpleName)

    val d = ctx.size
    while (chainAST.size > d) {
      chainAST.pop
    }

    val parentUriAST = chainAST.lastOption.map {
      l => chainAST.init.foldRight(new StringBuilder(l)) {
        (e, acc) => acc.append("/").append(e)
      }.result
    }.getOrElse("")

    val uriNonIndexedAST = parentUriAST + "/" + name
    val index = forksAST.getOrElse(uriNonIndexedAST, 0)
    forksAST += (uriNonIndexedAST -> (index + 1))
    chainAST.push(name + index)

    val relevantNode = domain.labels.contains(name)
    if (relevantNode) {
      depthGraph += 1

      while (chainGraph.size > depthGraph) {
        chainGraph.pop
      }

      val parentUriGraph = chainGraph.lastOption.map {
        l => chainGraph.init.foldRight(new StringBuilder(l)) {
          (e, acc) => acc.append("/").append(e)
        }.result
      }.getOrElse("")

      val uriNonIndexedGraph = parentUriGraph + "/" + name
      val index = forksGraph.getOrElse(uriNonIndexedGraph, 0)
      forksGraph += (uriNonIndexedGraph -> (index + 1))
      chainGraph.push(name + index)
 
      val uri = uriNonIndexedGraph + index

      val vertex = new AstVertex(uri, index, revisionRange, fileName, domain)

      val state = vertex.state.rangeStates(revisionRange)
      vertex.state = vertex.state.copy(
        rangeStates = vertex.state.rangeStates + (revisionRange ->
        (postprocess(state + (TypeLabel(false, name))) )))
      graph.addVertex(vertex)
      graph.addEdge(uri, new UpwardEdge("HAS-AST_PARENT", parentUriGraph))
      graph.addEdge(parentUriGraph, new DownwardEdge("HAS-AST_CHILD", uri))
    }
    if (relevantNode) { addCount += 1 } else { ignoreCount += 1 }
  }

  def leaveGeneric(node: Node, alias: Option[String] = None) {
    val name = alias.getOrElse(node.getClass.getSimpleName)
    val relevantNode = domain.labels.contains(name)
    if (relevantNode) { depthGraph -= 1 }
  }

  override def enterDefault(node: Node): Boolean = {
    enterGeneric(node)
    true
  }

  override def leaveDefault(node: Node): Node = {
    leaveGeneric(node)
    node
  }

  override def enterFunctionNode(node: FunctionNode): Boolean = {
    val (alias, name) = if (node.getKind == FunctionNode.Kind.SCRIPT) {
      (Some("Script"), fileName)
    } else (None, node.getName)
    def postProcess = { state: AnalysisState =>
      state + Name(true, name) + MethodParameterCount(true, node.getParameters.size)
    }
    enterGeneric(node, alias, postProcess)
    true
  }

  override def leaveFunctionNode(node: FunctionNode): Node = {
    val alias = if (node.getKind == FunctionNode.Kind.SCRIPT) {
      Some("Script")
    } else None
    leaveGeneric(node, alias)
    node
  }

}

