package ch.uzh.ifi.seal.lisa.module.mapping

object BrainMethodThresholds {
  val vertexCount = 390
  val mccPerVertex = 0.04
  val controlNesting = 2
  val variableCount = 8
}

