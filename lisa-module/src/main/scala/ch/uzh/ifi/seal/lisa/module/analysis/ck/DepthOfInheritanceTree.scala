package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

/* Depth of Inheritance Tree (DIT) is the maximum length of a path from a class
 * to a root class in the inheritance structure of a system. DIT measures how
 * many super-classes can affect a class. DIT is only applicable to
 * object-oriented systems. */
object DepthOfInheritanceTreeAnalysis extends Analysis with ParentCountHelper {
  case class CKDIT(persist: Boolean, n: Int) extends Data {
    def this() = this(false, 0)
  }

  override def start = { implicit domain => state =>
    if (state.is('class)) { state ? (new CKDITPacket(1), "HAS_SUB") }
    else state
  }

  class CKDITPacket(val count: Int) extends AnalysisPacket {
    override def collect = { implicit domain => state =>
      if (state.is('class)) {
        val n = if (state[CKDIT].n < count) { count } else { state[CKDIT].n }
        (state + CKDIT(true, n)) ? (new CKDITPacket(n + 1), "HAS_SUB")
      }
      else { state }
    }
  }
}

