package ch.uzh.ifi.seal.lisa.core.computation

import java.util.Date
import java.util.TimeZone

// a revision consists of sequential index, a rev string (sha) and references to
// the previous and next revisions before and after this one
case class Revision(n: Int, rev: String,
                    authorDate: Date, authorTz: TimeZone, authorName: String, authorEmail: String,
                    committerDate: Date, committerTz: TimeZone, committerName: String, committerEmail: String,
                    var prev: Option[Revision], var next: Option[Revision],
                    subject: String = "No Subject", message: String = "Empty Message")
                    extends Ordered[Revision] {
  override def toString: String = s"$n ($rev)"
  override def hashCode = n.hashCode
  override def equals(that: Any) = {
    that match {
      case r: Revision => r.n == n
      case _ => false
    }
  }
  private def formatDate(date: Date, tz: TimeZone): String = {
    val df = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mmXXX")
    df.setTimeZone(tz)
    df.format(date)
  }
  def getAuthorDateString(): String = { formatDate(authorDate, authorTz) }
  def getCommitterDateString(): String = { formatDate(committerDate, committerTz) }
  def compare(that: Revision) = this.n - that.n
}

case class RevisionRange(start: Revision, end: Revision) extends Ordered[RevisionRange] {
  def compare(that: RevisionRange) = {
    if (this.start.n != that.start.n) { this.start.n - that.start.n }
    else { this.end.n - that.end.n }
  }
  def contains(rev: Revision): Boolean = {
    this.start.n <= rev.n && this.end.n >= rev.n
  }
  def revisions(prefix: Revision = start): List[Revision] = {
    return prefix :: (prefix.next match {
       case Some(r) => if (r.n > end.n) Nil else revisions(r)
       case _ => Nil
    })
  }
  override def equals(that: Any) = {
    that match {
      case r: RevisionRange => r.start == start && r.end == end
      case _ => false
    }
  }
  override def toString: String = s"[${start.n}-${end.n}]"
  def toCommitsString: String = s"[${start.rev}-${end.rev}]"
  def size = end.n - start.n + 1
}

