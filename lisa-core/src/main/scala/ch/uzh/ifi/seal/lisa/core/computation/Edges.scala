package ch.uzh.ifi.seal.lisa.core.computation

import com.signalcollect._
/** The basic edge which all edges are inheriting.  *
  * Different subclasses of BaseEdge should describe different behavior and not
  * different types of edges which behave the same.
  *
  * @param relation the kind of relation depicted by this edge
  * @param t the targetId this edge is pointing at (inherited)
  */
abstract class BaseEdge(val relation: String, t: String) extends DefaultEdge(t) {
  type Source = BaseVertex
  def signal: GraphSignal = new EmptySignal()
}

/** An edge connecting AST vertices, bottom up (children point at their parents) */
class UpwardEdge(override val relation: String, t: String) extends BaseEdge(relation, t) {
  override def signal = {
    new AnalysisSignal(source.id, source.state.rangeStates.mapValues(_.analysesUp.filter( { case (packet, relationSelected) =>
      relationSelected.isEmpty || relation  == relationSelected
    }).map { case (packet, _) => packet }))
  }
}

/** An edge connecting AST vertices, top down (parents point at their children) */
class DownwardEdge(override val relation: String, t: String) extends BaseEdge(relation, t) {
  override def signal = {
    if (source.signalParsing) new EndRangeSignal(source.signalEnd)
    else {
      new AnalysisSignal(source.id, source.state.rangeStates.mapValues(_.analysesDown.filter( { case (packet, relationSelected) =>
        relationSelected.isEmpty || relation  == relationSelected
      }).map { case (packet, _) => packet }))
    }
  }
}

/** An edge connecting object parts (classes, methods, etc.) */
class ObjectEdge(override val relation: String, t: String) extends BaseEdge(relation, t) {
  override def signal = {
    if (source.signalParsing) new EndRangeSignal(source.signalEnd)
    else super.signal
    //else new AnalysisSignal(source.id, source.state.rangeStates.mapValues(_.analysesDown))
  }
}

/** An edge connection different objects */
class InterObjectEdge(override val relation: String, t: String) extends BaseEdge(relation, t) {
  override def signal = {
    new AnalysisSignal(source.id, source.state.rangeStates.mapValues(_.analysesInterObject.filter( { case (packet, relationSelected) =>
      relationSelected.isEmpty || relation  == relationSelected
    }).map { case (packet, _) => packet }))
  }
}

