package ch.uzh.ifi.seal.lisa.core.public

import java.nio.ByteBuffer
import java.nio.CharBuffer
import com.signalcollect.GraphEditor
import ch.uzh.ifi.seal.lisa.core.computation._

/** Parser interface
  *
  * A "parser" can be anything that loads data from a ByteBuffer into the graph.
  */
abstract class Parser {

  /** A list of suffixes indicating which file types the parser supports */
  val suffixes: List[String]

  /** A list of regex strings indicating filenames to exclude */
  val excludes = List[String]()

  /** Parse the given files into the given Graph
    *
    * @param files a list of tuples with identifiers and contents of the files
    * @param graph the GraphEditor provided for modifying the graph
    * @param rev the Revision these files belong to
    * @see ch.uzh.ifi.seal.lisa.module.parser.JDKJavaParser and
    * @see ch.uzh.ifi.seal.lisa.antlr.AntlrParser for example implementations
    * @see com.signalcollect.GraphEditor for further details
    * @return total number of parsed lines of code
    */
  def parse(files: List[(String,ByteBuffer)], graph: GraphEditor[Any, Any],
            rev: Revision): ParseStats = { ParseStats() }

  def postprocess(graph: GraphEditor[Any, Any], v: BaseVertex) = {}

  def willRead(filename: String): Boolean = {
    suffixes.exists(filename.endsWith(_)) && !excludes.exists(filename.endsWith(_))
  }

  import java.nio.charset.StandardCharsets
  def bytesToChars(fileBytes: ByteBuffer): CharBuffer = {
    StandardCharsets.UTF_8.decode(fileBytes)
  }

}

case class ParseStats(lineCount: Long = 0,
                      nodeAddCount: Long = 0,
                      nodeIgnoreCount: Long = 0,
                      errorCount: Long = 0) {
  def add(other: ParseStats): ParseStats = {
    ParseStats(lineCount + other.lineCount,
               nodeAddCount + other.nodeAddCount,
               nodeIgnoreCount + other.nodeIgnoreCount,
               errorCount + other.errorCount)
  }
}


