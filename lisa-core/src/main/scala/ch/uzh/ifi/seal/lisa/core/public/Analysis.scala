package ch.uzh.ifi.seal.lisa.core.public

import ch.uzh.ifi.seal.lisa.core.computation._

/** Basic trait for Analyses with start/collectEdit/collect functions doing nothing
  *
  * Subclasses of this may accept any parameters and can thus store local data
  * or additional functions. To process information, Analysis subclasses can
  * implement the collectEdit and collect functions, which are from the
  * deliverSignal and collect functions of a vertex.
  */
trait Analysis {
  /** Start the Analysis given a VertexContext and an AnalysisState
    *
    * Subclasses need to override this method to determine whether an analysis
    * should be started. Analyses can also modify state at this point already.
    *
    * @param v the vertex context (id and vertex type)
    * @param state the AnalysisState to be inspected/modified
    */
  def start = { implicit d: Domain =>
                implicit state: AnalysisState =>
                state }

  /** call the start function of the given analysis on the given vertex */
  final def launch(v: BaseVertex, analysis: Analysis) {
    v.state = v.state.copy(rangeStates =
      v.state.rangeStates.mapValues { s =>
        analysis.start(v.domain)(s)
      }
    )
  }

}

abstract class AnalysisPacket {
  /** Given a domain and state, change the state
    *
    * Implements the main course of the Analysis. It may modify the vertex state
    * by setting/getting any properties, because State extends Dynamic. It can
    * also spawn a new Analysis to be sent out or simply forward the incoming
    * analysis.
    *
    * @param v the vertex collecting a signal
    * @param state the AnalysisState to be inspected/modified
    */
  def collect = { implicit d: Domain =>
                  implicit state: AnalysisState =>
                  state }

  /** Given a domain and state, change the state and edit the graph
    *
    * While most processing should be done in collect, analyses can use this
    * function to execute actions before the collect step. They should implement
    * this method if they need to use a GraphEditor, for example to create new
    * vertices. However, they cannot modify state during this step.
    *
    * @param v the vertex collecting a signal
    * @param state the AnalysisState to be inspected
    * @param graph a GraphEditorWrapper which can be used to modify the graph
    */
  def collectEdit = { implicit d: Domain =>
                      implicit state: AnalysisState =>
                      state }

}


