package ch.uzh.ifi.seal.lisa.core.misc

import java.io.File
import java.io.InputStream
import java.io.BufferedInputStream
import java.io.FileInputStream
import java.nio.file._
import java.nio.file.attribute._
import java.io.IOException
import scala.collection.mutable.{Map => MMap}

object Profiling {
  def time[R](block: => R): (R, Long) = {
    val t0 = System.nanoTime()
    val result = block
    val t1 = System.nanoTime()
    (result, (t1 - t0))
  }
}

object RuntimeStats {
  var periodicStats = false
  private val stats = MMap[String,MMap[String,Any]]()
  def getStats(uid: String): MMap[String,Any] = {
    stats.getOrElseUpdate(uid, MMap[String,Any]())
  }
  def clearStats(uid: String) = {
    stats.getOrElseUpdate(uid, MMap[String,Any]()).clear()
  }
}

trait RuntimeStats {
  def stats()(implicit uid: String) = RuntimeStats.getStats(uid)
  def listStat(key: String, value: Any)(implicit uid: String) {
    synchronized {
      val s = stats.getOrElse(key, List[Any]()).asInstanceOf[List[Any]]
      stats += (key -> (s :+ value))
    }
  }
  def appendStat(key: String, value: Any, index: Int = 0)(implicit uid: String) {
    val nextKey = s"${key}_${index}"
    if (stats.contains(nextKey)) {
      appendStat(key, value, index + 1)
    }
    else {
      stats += (nextKey -> value)
    }
  }
  def clearStats()(implicit uid: String) {
    RuntimeStats.clearStats(uid)
  }
}


object FileTools {
  def removeRecursively(path: Path) {
    Files.walkFileTree(path, new RemovalFileVisitor)
  }
  def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit) {
    val p = new java.io.PrintWriter(f)
    try { op(p) } finally { p.close() }
  }

  def contentEquals(file1: String, file2: String): Boolean = {
    contentEquals(new File(file1), new File(file2))
  }

  /* from https://github.com/apache/commons-io/blob/master/src/main/java/org/apache/commons/io/FileUtils.java#L722 */
  def contentEquals(file1: File, file2: File): Boolean = {
    val file1Exists = file1.exists()
    if (file1Exists != file2.exists()) {
      false
    }

    if (!file1Exists) {
      // two not existing files are equal
      true
    }

    if (file1.isDirectory() || file2.isDirectory()) {
      // don't want to compare directory contents
      throw new IOException("Can't compare directories, only files")
    }

    if (file1.length() != file2.length()) {
      // lengths differ, cannot be equal
      false
    }

    if (file1.getCanonicalFile().equals(file2.getCanonicalFile())) {
      // same file
      true
    }

    val input1 = new FileInputStream(file1)
    val input2 = new FileInputStream(file2)
    contentEquals(input1, input2)
  }

  def EOF = -1
  /* from https://github.com/apache/commons-io/blob/master/src/main/java/org/apache/commons/io/IOUtils.java#L659 */
  def contentEquals(input1: InputStream, input2: InputStream): Boolean = {
      if (input1 == input2) {
        true
      }
      val bInput1 = input1 match {
        case i: BufferedInputStream => i
        case _ => new BufferedInputStream(input1)
      }
      val bInput2 = input2 match {
        case i: BufferedInputStream => i
        case _ => new BufferedInputStream(input2)
      }
      var ch = bInput1.read()
        while (EOF != ch) {
          val ch2 = bInput2.read()
            if (ch != ch2) {
              return false
            }
          ch = bInput1.read()
        }

      val ch2 = bInput2.read()
      bInput1.close()
      bInput2.close()
      ch2 == EOF
    }

}

// Adapted for scala from
// http://stackoverflow.com/questions/779519/delete-files-recursively-in-java/8685959#8685959
class RemovalFileVisitor extends SimpleFileVisitor[java.nio.file.Path] {
  override def visitFile(file: Path,
                         attrs: BasicFileAttributes): FileVisitResult = {
    Files.delete(file)
    return FileVisitResult.CONTINUE
  }

  override def visitFileFailed(file: Path,
                               exc: IOException): FileVisitResult = {
    // try to delete the file anyway, even if its attributes
    // could not be read, since delete-only access is
    // theoretically possible
    Files.delete(file)
    return FileVisitResult.CONTINUE
  }

  override def postVisitDirectory(dir: Path,
                                  exc: IOException): FileVisitResult = {
    if (exc == null) {
      Files.delete(dir)
      return FileVisitResult.CONTINUE
    }
    else {
      // directory iteration failed propagate exception
      throw exc
    }
  }
}

