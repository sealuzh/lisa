package ch.uzh.ifi.seal.lisa.core.computation

/** A signal transmitting a list of analyses from one vertex to another.
  *
  * @param sourceVertexId the id of the source vertex
  * @param rangeAnalyses a map of the analyses to be applied for each range
  */
trait GraphSignal

class EmptySignal extends GraphSignal

class AnalysisSignal(
  val sourceVertexId: String,
  val rangeAnalyses: Map[RevisionRange, List[AnalysisPacket]]) extends GraphSignal {
  override def toString(): String = {
    s"GraphSignal source: $sourceVertexId, " +
    s"carrying ${rangeAnalyses.size} analyses"
  }
}

class EndRangeSignal(val end: List[Option[Revision]]) extends GraphSignal


