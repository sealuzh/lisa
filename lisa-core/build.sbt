name := "lisa-core"

version := "0.2.5"

libraryDependencies ++= {
  Seq(
    "org.specs2" %% "specs2-core" % "3.9.5" % "test",
    "com.typesafe" % "config" % "1.3.3",
    "com.typesafe.akka" %% "akka-actor" % "2.5.17",
    "com.typesafe.akka" %% "akka-slf4j" % "2.5.17",
    "ch.qos.logback" % "logback-classic" % "1.2.3",
    "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0",
    "org.eclipse.jgit" % "org.eclipse.jgit" % "5.3.0.201901162155-m1",
    "com.github.jbellis" % "jamm" % "0.3.2",
    "io.spray" %%  "spray-json" % "1.3.3",
    "org.ow2.asm" % "asm" % "7.0",
    "org.webjars" % "d3js" % "5.5.0",
    "com.signalcollect" %% "signal-collect" % "8.0.12" from "https://files.ifi.uzh.ch/seal/lisa/jar/signal-collect-assembly-8.0.12.jar"
  )
}

unmanagedJars in Compile += file(System.getProperty("java.home").dropRight(3)+"lib/tools.jar")

resolvers ++= Seq(
  "Eclipse Repository" at "https://repo.eclipse.org/content/groups/releases/",
  "Maven Central Server" at "http://repo1.maven.org/maven2"
)

