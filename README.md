LISA is a highly generic multi-revision graph database and computation engine
designed for analyzing software artifacts that exist in many revisions of a
project.

It avoids redundancy when analyzing multiple revisions by loading artifacts from
different revisions into a shared representation where only the smallest delta
between revisions requires extra space. Computations also run on entire ranges
of revisions where no change occured at the sub-graph level.

For more information, please refer to [this paper](http://tiny.uzh.ch/10x)

LISA works on Linux and Mac.

### Table of Contents
[TOC]

### Quick-Start
The [lisa-quickstart](https://bitbucket.org/sealuzh/lisa-quickstart/src)
repository contains information on how to try out LISA or to use it as a
library. If you're not interested in the implementation or extension of LISA
itself, it provides a better starting point.

### Project structure
LISA is split into multiple sub-projects. The `lisa-core` contains the generic
framework components which exist independently of any language or analysis.
The `lisa-antlr` project contains grammars used to create ANTLR-based parsers
and the `lisa-module` project contains various parsers and pre-composed
analyses.

### Prerequisites
The following tools need to be installed in order to build and use LISA core:

 * sbt >= 0.13.12
 * JRE >= 8

If you want to use the native JDK JavaParser instead of the ANTLR generated
parser you also need to install the JDK and not just the JRE.

### Building & Testing
To build LISA, you can use sbt:

    sbt compile

To run all default tests, run

    sbt test

To build self-contained jar files for each sub-project, run

    sbt assembly


### Using LISA as a library
On its own, lisa-core does not provide any parsers or analyses. It's possible to
either use existing ones from lisa-module or provide new, custom built parsers
and analyses.

To use only `lisa-core` as a library, place the following in your `build.sbt`

```
libraryDependencies += "lisa-core" % "lisa-core" % "0.2.5" from "https://files.ifi.uzh.ch/seal/lisa/jar/lisa-core-assembly-0.2.5.jar"
```

To use the parsers and analyses contained in `lisa-module`, add the following instead:

```
libraryDependencies += "lisa-module" % "lisa-module" % "0.2.5" from "https://files.ifi.uzh.ch/seal/lisa/jar/lisa-module-assembly-0.2.5.jar"
```

In the following example, we want to analyze a project that contains mixed C#
and Javascript code. This example can be run by executing

```
sbt ";project lisa-module ;test:runMain ch.uzh.ifi.seal.lisa.misc.ReadmeExample"
```

    :::scala
    import ch.uzh.ifi.seal.lisa.core.public.LisaComputation
    import ch.uzh.ifi.seal.lisa.core.source.GitAgent
    import ch.uzh.ifi.seal.lisa.module.parser.{AntlrCSharpParser,AntlrJavascriptParser}
    import ch.uzh.ifi.seal.lisa.module.analysis.UniversalAnalysisSuite
    import ch.uzh.ifi.seal.lisa.module.persistence.CSVPersistence

    object ReadmeExample extends App {
      // a unique identifier used for to name log files and actor systems
      implicit val uid = "lisa-jint"
      // the parser we want to use
      val parsers = List(AntlrCSharpParser, AntlrJavascriptParser)
      // provide access to the sources
      val sources = new GitAgent(parsers,
        url = "https://github.com/sebastienros/jint.git",
        localDirPath = "/tmp/example",
        start = Some("1db58da6bb1b931b1cfc7d454d18e25061ae3cb7"),
        end = Some("e6ea95d427b2ed7a13112dfa2ca26d60ac71249e")
      )
      // the analyses we want to run. Multiple suites can be merged using '+'
      val analyses = UniversalAnalysisSuite
      // how we want to persist the results
      val persistence = new CSVPersistence("/tmp/jint-results")
      // prepare a computation
      val c = new LisaComputation(sources, analyses, persistence)
      // run the computation
      c.execute
    }

### Writing new analyses
To formulate new analyses, it's best to first familiarize with the
Signal/Collect computation paradigm: https://uzh.github.io/signal-collect/

Refer to existing analyses contained in `lisa-module`, for example [MccAnalysis.scala](https://bitbucket.org/sealuzh/lisa/src/master/lisa-module/src/main/scala/ch/uzh/ifi/seal/lisa/module/analysis/object-oriented/MccAnalysis.scala) to observe how they are implemented.

In general, one needs to define a case class to hold the analysis data (e.g.
`Mcc` on line 15 of `MccAnalysis`), a function that determines how the analysis
gets started (like on line 19 of `MccAnalysis`), and a class which represents
the message which is transmitted between vertices (`MccPacket` on line 25). This
class needs to specify how the message is processed (`collect` on line 26).

### Memory requirements
Depending on the size of the projects to be analyzed, LISA requires a large
amount of memory. Be sure to provide enough memory to the JVM by editing
`.sbtopts` and setting suitable values for `Xmx` and `Xms`.

### License

Copyright 2019 Carol V. Alexandru

Licensed under the Apache License, Version 2.0 (the "License");
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

